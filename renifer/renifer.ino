/*
gnd - czarny - rezystor 1kOhm - fotorezystor nóżka 1
fotorezystor nóżka 1 - żółty - A0
fotorezystor nóżka 2 - pomarańczowy - 5V
*/

    //int ledPin = 2;
    //odczytana wartość
    int val = 0;
    int black = 13;
    int orange = 3;
    boolean isHigh = false;
    int threshold;
     
    void setup()
    {
      //pinMode(ledPin, OUTPUT);
      
      pinMode(orange, OUTPUT);  
      digitalWrite(orange, LOW); 
      pinMode(black, INPUT);  
      digitalWrite(black, LOW); 
      delay(100);
      threshold = analogRead(A0) / 2;
      Serial.begin(57600);
      Serial.println(threshold);
    }
     
    void loop()
    {
      val = analogRead(A0);
      Serial.println(val);
      if (val < threshold) {
        //digitalWrite(ledPin, HIGH);
        digitalWrite(orange, HIGH); 
        isHigh = true;
      } else {        
        digitalWrite(orange, LOW);
        if (isHigh) {
          delay(100);
          digitalWrite(orange, HIGH);
          delay(100);
          digitalWrite(orange, LOW);  
          isHigh = false;
        }
        //digitalWrite(ledPin, LOW);        
      }
      delay(300);
    }
